//
//  FavoritesManager.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 09.11.2022..
//

import Foundation
import SwiftUI

class FavoritesManager: ObservableObject {
    static let shared = FavoritesManager(persistenceService: ServiceFactory.persistenceService)
    
    private let persistenceService: PersistenceProtocol
    @Published var favorites: [Int] = []

    private init(persistenceService: PersistenceProtocol) {
        self.persistenceService = persistenceService
        favorites = persistenceService.favoritesData.favorites
    }

    func addFavorite(favorite: Int) {
        self.favorites.append(favorite)
        persistenceService.favoritesData.favorites.append(favorite)
    }
    
    func removeFavorite(favorite: Int) {
        if let index = favorites.firstIndex(of: favorite) {
            self.favorites.remove(at: index)
            persistenceService.favoritesData.favorites.remove(at: index)
        }
    }
    
    func isFavorite(favorite: Int) -> Bool {
        return self.favorites.contains(favorite) ? true : false
    }
}
