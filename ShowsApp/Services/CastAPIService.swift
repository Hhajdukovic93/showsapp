//
//  CastAPIService.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import Foundation

protocol CastAPIProtocol: AnyObject {
    func fetchCastData(showID: Int, completionHandler: @escaping (Result<[CastAPIResponse], Error>) -> Void)
}

class CastAPIService: CastAPIProtocol {
    private let networkingService: NetworkingProtocol = ServiceFactory.networkingService
    
    func fetchCastData(showID: Int, completionHandler: @escaping (Result<[CastAPIResponse], Error>) -> Void) {
        let urlString = "https://api.tvmaze.com/shows/\(showID)/cast"
        let url = URL(string: urlString)
        networkingService.fetchJSON(from: url, completionHandler: completionHandler)
    }
}
