//
//  ServiceFactory.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import Foundation

enum ServiceFactory {
    static var networkingService: NetworkingProtocol {
        NetworkingService()
    }
    
    static var showAPIService: ShowAPIProtocol {
        ShowAPIService()
    }
    
    static var scheduleAPIService: ScheduleAPIProtocol {
        ScheduleAPIService()
    }
    
    static var searchAPIService: SearchAPIProtocol {
        SearchAPIService()
    }
    
    static var casrPIService: CastAPIProtocol {
        CastAPIService()
    }
    
    static var persistenceService: PersistenceProtocol {
        PersistenceService()
    }
}
