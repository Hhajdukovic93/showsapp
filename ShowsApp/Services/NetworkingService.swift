//
//  NetworkingService.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import Foundation

enum NetworkingError: Error {
    case badURL
    case noInternet
    case badJSON
}

protocol NetworkingProtocol: AnyObject {
    func fetchJSON<T>(from url: URL?, completionHandler: @escaping (Result<T, Error>) -> Void) where T: Decodable
}

class NetworkingService: NetworkingProtocol {
    func fetchJSON<T>(from url: URL?, completionHandler: @escaping (Result<T, Error>) -> Void) where T : Decodable {
        guard let url = url else {
            return completionHandler(.failure(NetworkingError.badURL))
        }
        guard let data = try? Data(contentsOf: url) else {
            return completionHandler(.failure(NetworkingError.noInternet))
        }
        guard let decodedData = try? JSONDecoder().decode(T.self, from: data) else {
            return completionHandler(.failure(NetworkingError.badJSON))
        }
        
        completionHandler(.success(decodedData))
    }
}
