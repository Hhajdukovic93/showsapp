//
//  PersistenceService.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 01.11.2022..
//

import Foundation

protocol PersistenceProtocol: AnyObject {
    var favoritesData: FavoritesData { get set }
}

class PersistenceService: PersistenceProtocol {
    var favoritesData: FavoritesData {
        get {
            UserDefaults.standard.load() ?? FavoritesData.defaultData
        }
        set {
            UserDefaults.standard.save(newValue)
        }
    }
}
