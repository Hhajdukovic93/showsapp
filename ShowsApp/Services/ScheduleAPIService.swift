//
//  ScheduleAPIService.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import Foundation

protocol ScheduleAPIProtocol: AnyObject {
    func fetchScheduleData(completionHandler: @escaping (Result<[ScheduleAPIResponse], Error>) -> Void)
}

class ScheduleAPIService: ScheduleAPIProtocol {
    private let networkingService: NetworkingProtocol = ServiceFactory.networkingService
    
    func fetchScheduleData(completionHandler: @escaping (Result<[ScheduleAPIResponse], Error>) -> Void) {
        let urlString = "https://api.tvmaze.com/schedule?country=US&date=2014-12-01"
        let url = URL(string: urlString)
        networkingService.fetchJSON(from: url, completionHandler: completionHandler)
    }
}
