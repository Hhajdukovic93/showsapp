//
//  SearchAPIService.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import Foundation

protocol SearchAPIProtocol: AnyObject {
    func fetchSearchData(searchFor: String, completionHandler: @escaping (Result<[SearchAPIResponse], Error>) -> Void)
}

class SearchAPIService: SearchAPIProtocol {
    private let networkingService: NetworkingProtocol = ServiceFactory.networkingService
    
    func fetchSearchData(searchFor: String, completionHandler: @escaping (Result<[SearchAPIResponse], Error>) -> Void) {
        let urlString = "https://api.tvmaze.com/search/shows?q=\(searchFor)"
        let url = URL(string: urlString)
        networkingService.fetchJSON(from: url, completionHandler: completionHandler)
    }
}
