//
//  ShowAPIService.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import Foundation

protocol ShowAPIProtocol: AnyObject {
    func fetchShowData(showID: Int, completionHandler: @escaping (Result<ShowAPIResponse, Error>) -> Void)
}

class ShowAPIService: ShowAPIProtocol {
    private let networkingService: NetworkingProtocol = ServiceFactory.networkingService
    
    func fetchShowData(showID: Int, completionHandler: @escaping (Result<ShowAPIResponse, Error>) -> Void) {
        let urlString = "https://api.tvmaze.com/shows/\(showID)"
        let url = URL(string: urlString)
        networkingService.fetchJSON(from: url, completionHandler: completionHandler)
    }
}
