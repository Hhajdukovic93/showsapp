//
//  FavoritesData.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 01.11.2022..
//

import Foundation

struct FavoritesData: Codable {
    var favorites: [Int]
    
    static var defaultData: Self {
        FavoritesData(favorites: [])
    }
}
