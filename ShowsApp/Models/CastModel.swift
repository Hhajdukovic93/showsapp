//
//  CastModel.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 03.11.2022..
//

import Foundation

struct CastModel: Identifiable {
    let id: Int
    let realName: String
    let characterName: String
    let imageURL: String
}
