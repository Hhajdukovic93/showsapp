//
//  ShowModel.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import Foundation

struct ShowModel: Identifiable {
    let id: Int
    let name: String
    let imageURL: String
    let rating: Double?
    let premiered: String?
    let summary: String?
    let cast: [CastModel]?
}
