//
//  ScheduleModel.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 09.11.2022..
//

import Foundation

struct ScheduleModel: Identifiable {
    let id: Int
    let name: String
    let imageURL: String
    let schedule: String?
    let premiered: String?
    let summary: String?
    let cast: [CastModel]?
}
