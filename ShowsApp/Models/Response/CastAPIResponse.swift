//
//  CastAPIResponse.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import Foundation

struct CastAPIResponse: Codable {
    let person: RealPerson
    let character: MovieCharacter
}

struct RealPerson: Codable {
    let name: String
    let image: imageType
}

struct MovieCharacter: Codable {
    let name: String
}
