//
//  SearchAPIResponse.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import Foundation

struct SearchAPIResponse: Codable {
    let show: ShowData
}

struct ShowData: Codable {
    let id: Int
    let name: String?
    let premiered: String?
    let image: imageType?
    let summary: String?
}

struct imageType: Codable {
    let medium: String
    let original: String
}
