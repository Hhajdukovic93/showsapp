//
//  ShowAPIResponse.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import Foundation

struct ShowAPIResponse: Codable {
    let id: Int
    let name: String
    let image: imageTypes
    let rating: avarageRating
    let summary: String
}

struct imageTypes: Codable {
    let medium: String
    let original: String
}

struct avarageRating: Codable {
    let average: Double
}
