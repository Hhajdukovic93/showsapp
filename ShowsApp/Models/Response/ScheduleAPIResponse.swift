//
//  ScheduleAPIResponse.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import Foundation

struct ScheduleAPIResponse: Codable {
    let id: Int
    let name: String
    let airtime: String
    let airstamp: String
    let show: ShowData
}
