//
//  Coordinator.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 26.10.2022..
//

import Foundation
import UIKit

protocol Coordinator: AnyObject {
    func start() -> UIViewController
}
