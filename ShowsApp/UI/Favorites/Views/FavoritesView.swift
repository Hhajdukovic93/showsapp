//
//  FavoritesView.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 26.10.2022..
//

import SwiftUI

struct FavoritesView: View {
    @ObservedObject var favsManager = FavoritesManager.shared
    
    let layout = [
        GridItem(.adaptive(minimum: 150, maximum: 160))
    ]
    
    var body: some View {
        VStack {
            if !favsManager.favorites.isEmpty {
                ScrollView {
                    LazyVGrid(columns: layout) {
                        ForEach(favsManager.favorites, id: \.self) { key in
                            NavigationLink {
                                DetailsView(id: key)
                            } label: {
                                FavoritesCardView(id: key)
                            }
                        }
                    }
                }
            } else {
                ZStack {
                    Color.black
                    Text("Feel free to choose your Favorite shows")
                        .foregroundColor(.white)
                }
            }
        }
        .background(.black)
    }
}

struct FavoritesView_Previews: PreviewProvider {
    static var previews: some View {
        FavoritesView()
    }
}
