//
//  FavoritesCardView.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import SwiftUI

struct FavoritesCardView: View {
    @ObservedObject var favsManager = FavoritesManager.shared
    @EnvironmentObject private var favoritesVM: FavoritesViewModel
    @StateObject private var searchVM = SearchViewModel(showAPISerivce: ShowAPIService(), searchAPIService: SearchAPIService(), castAPIService: CastAPIService())
    var id: Int

    var body: some View {
        ZStack(alignment: .topLeading) {
            AsyncImage(url: URL(string: searchVM.show.imageURL)) { image in
                image
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    
            } placeholder: {
                Color.gray
            }
            ZStack {
                Rectangle()
                    .frame(width: 27, height: 27)
                    .overlay(RoundedRectangle(cornerRadius: 5).stroke(Color.gray, lineWidth: 2))
                Image(systemName: "heart.fill")
                    .foregroundColor(.yellow)
            }
            .foregroundColor(.black)
            .padding()
            .onTapGesture {
                favsManager.removeFavorite(favorite: id)
            }
        }
        .background(.black)
        .onAppear {
            Task {
                searchVM.getShow(id: id)
            }
        }
    }
}

struct FavoritesCardView_Previews: PreviewProvider {
    static var previews: some View {
        FavoritesCardView(id: 1)
            .environmentObject(FavoritesViewModel())
    }
}
