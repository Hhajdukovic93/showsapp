//
//  FavoritesCoordinator.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 26.10.2022..
//

import UIKit
import SwiftUI

class FavoritesCoordinator: Coordinator {
    
    func start() -> UIViewController {
        let vc = UIHostingController(rootView: FavoritesView())
        vc.tabBarItem.title = "Favorites"
        vc.tabBarItem.image = UIImage(systemName: "heart.fill")
        return vc
    }
    
}
