//
//  HomeCoordinator.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 26.10.2022..
//

import UIKit
import SwiftUI

class HomeCoordinator: Coordinator {
    
    func start() -> UIViewController {
        let vc = UIHostingController(rootView: HomeView())
        vc.tabBarItem.title = "Home"
        vc.tabBarItem.image = UIImage(systemName: "house.fill")
        return vc
    }
    
}
