//
//  ScheduleCardView.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import SwiftUI

struct ScheduleCardView: View {
    @EnvironmentObject var homeVM: HomeViewModel
    @ObservedObject var favsManager = FavoritesManager.shared
    var id: Int
    
    var body: some View {
        VStack(alignment: .leading) {
            ZStack(alignment: .topLeading) {
                AsyncImage(url: URL(string: homeVM.schedule[id]?.imageURL ?? "")) { image in
                    image
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                } placeholder: {
                    Color.gray
                }
                ZStack {
                    Rectangle()
                        .frame(width: 33, height: 33)
                        .overlay(RoundedRectangle(cornerRadius: 5).stroke(Color.gray, lineWidth: 2))
                    if favsManager.isFavorite(favorite: id) {
                        Image(systemName: "heart.fill")
                            .foregroundColor(.yellow)
                    } else {
                        Image(systemName: "heart.fill")
                            .foregroundColor(.gray)
                    }
                }
                .foregroundColor(.black)
                .onTapGesture {
                    favsManager.isFavorite(favorite: id) ? favsManager.removeFavorite(favorite: id) : favsManager.addFavorite(favorite: id)
                }
            }
            HStack() {
                Spacer()
                Text(homeVM.schedule[id]?.schedule ?? "")
                    .font(.caption2)
            }
            Text(homeVM.schedule[id]?.name ?? "")
                .padding(.leading)
                .font(.caption)
                .lineLimit(1)
        }
        .background(.gray)
        .foregroundColor(.white)
        .frame(width: 132, height: 242)
        .padding(.trailing)
    }
}

struct ScheduleCardView_Previews: PreviewProvider {
    static var previews: some View {
        ScheduleCardView(id: 1)
            .environmentObject(HomeViewModel(showAPISerivce: ShowAPIService(), scheduleAPIService: ScheduleAPIService()))
    }
}
