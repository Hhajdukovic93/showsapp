//
//  ShowCardView.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import SwiftUI

struct ShowCardView: View {
    @EnvironmentObject var homeVM: HomeViewModel
    @ObservedObject var favsManager = FavoritesManager.shared
    var id: Int
    
    var body: some View {
        VStack(alignment: .leading) {
            ZStack(alignment: .topLeading) {
                AsyncImage(url: URL(string: homeVM.shows[id]?.imageURL ?? "")) { image in
                    image
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                } placeholder: {
                    Color.gray
                }
                ZStack {
                    Rectangle()
                        .frame(width: 44, height: 44)
                        .overlay(RoundedRectangle(cornerRadius: 5).stroke(Color.gray, lineWidth: 2))
                    if favsManager.isFavorite(favorite: id) {
                        Image(systemName: "heart.fill")
                            .foregroundColor(.yellow)
                    } else {
                        Image(systemName: "heart.fill")
                            .foregroundColor(.gray)
                    }
                }
                .foregroundColor(.black)
                .onTapGesture {
                    favsManager.isFavorite(favorite: id) ? favsManager.removeFavorite(favorite: id) : favsManager.addFavorite(favorite: id)
                }
            }
            HStack() {
                Image(systemName: "star.fill")
                    .foregroundColor(.yellow)
                Text(String(homeVM.shows[id]?.rating ?? 0.0))
            }
            .padding(.leading)
            .font(.caption)
            Text(homeVM.shows[id]?.name ?? "")
                .padding(.leading)
                .font(.callout)
                .lineLimit(1)
        }
        .background(.gray)
        .foregroundColor(.white)
        .frame(width: 176, height: 308)
        .padding(.trailing)
    }
}

struct ShowCardView_Previews: PreviewProvider {
    static var previews: some View {
        ShowCardView(id: 1)
            .environmentObject(HomeViewModel(showAPISerivce: ShowAPIService(), scheduleAPIService: ScheduleAPIService()))
    }
}



