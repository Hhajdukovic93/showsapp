//
//  HomeView.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 26.10.2022..
//

import SwiftUI

struct HomeView: View {
    @StateObject private var homeVM = HomeViewModel(showAPISerivce: ShowAPIService(), scheduleAPIService: ScheduleAPIService())
    
    var body: some View {
        VStack {
            VStack(alignment: .leading) {
                HStack {
                    Text("Shows")
                        .font(.title)
                        .foregroundColor(.gray)
                    Spacer()
                    Text("show all")
                        .foregroundColor(.yellow)
                }
                .padding(.trailing)
                ScrollView(.horizontal)  {
                    HStack() {
                        ForEach(homeVM.shows.keys.sorted(), id: \.self) { showID in
                            NavigationLink {
                                DetailsView(id: showID)
                            } label: {
                                ShowCardView(id: showID)
                                    .environmentObject(self.homeVM)
                            }
                        }
                    }
                }
            }
            VStack(alignment: .leading)  {
                HStack {
                    HStack {
                        Text("Schedule")
                            .font(.title2)
                            .foregroundColor(.gray)
                        Spacer()
                        Text("show all")
                            .foregroundColor(.yellow)
                    }
                    .padding(.trailing)
                }
                ScrollView(.horizontal)  {
                    HStack() {
                        ForEach(homeVM.schedule.keys.sorted(), id: \.self) { showID in
                            NavigationLink {
                                DetailsView(id: showID)
                            } label: {
                                ScheduleCardView(id: showID)
                                    .environmentObject(self.homeVM)
                            }
                        }
                    }
                }
            }
            .padding(.top)
            Spacer()
        }
        .padding(.leading)
        .onAppear {
            Task {
                if !homeVM.isShowsTriggered {
                    homeVM.getShows()
                }
                if !homeVM.isScheduleTriggered {
                    homeVM.getSchedule()
                }
            }
        }
        .background(.black)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
