//
//  HomeViewModel.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import Foundation

class HomeViewModel: ObservableObject {
    private let showAPISerivce: ShowAPIProtocol
    private let scheduleAPIService: ScheduleAPIProtocol
    @Published var shows: [Int : ShowModel] = [:]
    @Published var schedule: [Int : ScheduleModel] = [:]
    var isShowsTriggered: Bool = false
    var isScheduleTriggered: Bool = false
    
    init(showAPISerivce: ShowAPIProtocol, scheduleAPIService: ScheduleAPIProtocol) {
        self.showAPISerivce = showAPISerivce
        self.scheduleAPIService = scheduleAPIService
    }
    
    func getShows() {
        for i in 1...20 {
            DispatchQueue.global(qos: .background).async { [weak self] in
                guard let self = self else { return }
                self.showAPISerivce.fetchShowData(showID: i, completionHandler: { [self] response in
                    DispatchQueue.main.async {
                        switch response {
                        case.success(let result):
                            let summaryWithoutTags = result.summary.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
                            let newShow = ShowModel(id: result.id, name: result.name, imageURL: result.image.medium, rating: result.rating.average, premiered: nil, summary: summaryWithoutTags, cast: nil)
                            self.shows[result.id] = newShow
                        case .failure(let error):
                            print("Error: \(error)")
                        }
                    }
                    
                })
            }
        }
        self.isShowsTriggered = true
    }
    
    func getSchedule() {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.scheduleAPIService.fetchScheduleData(completionHandler: { [self] response in
                DispatchQueue.main.async {
                    switch response {
                    case.success(let result):
                        var count = 0
                        if count < 20 {
                            for res in result {
                                let summaryWithoutTags = res.show.summary?.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
                                let newShow = ScheduleModel(id: res.show.id, name: res.show.name ?? "", imageURL: res.show.image?.medium ?? "", schedule: res.airtime, premiered: nil, summary: summaryWithoutTags, cast: nil)
                                self.schedule[res.show.id] = newShow
                            }
                            count += 1
                        }
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }
            })
        }
        self.isScheduleTriggered = true
    }
}
