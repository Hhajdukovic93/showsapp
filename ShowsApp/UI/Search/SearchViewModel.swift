//
//  SearchViewModel.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 02.11.2022..
//

import Foundation

class SearchViewModel: ObservableObject {
    private let showAPISerivce: ShowAPIProtocol
    private let searchAPIService: SearchAPIProtocol
    private let castAPIService: CastAPIProtocol
    @Published var show = ShowModel(id: 1, name: "", imageURL: "", rating: nil, premiered: "", summary: "", cast: nil)
    @Published var searchResults: [ShowModel] = []
    @Published var cast: [CastModel] = []
    let group = DispatchGroup()
    
    init(showAPISerivce: ShowAPIProtocol, searchAPIService: SearchAPIProtocol, castAPIService: CastAPIProtocol) {
        self.showAPISerivce = showAPISerivce
        self.searchAPIService = searchAPIService
        self.castAPIService = castAPIService
    }

    func search(text: String) {
        self.searchResults = []
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.searchAPIService.fetchSearchData(searchFor: text, completionHandler: { [self] response in
                DispatchQueue.main.async {
                    switch response {
                    case.success(let result):
                        for res in result {
                            self.fetchCast(showID: res.show.id)
                            self.group.notify(queue: .main) {
                                let newShow = ShowModel(id: res.show.id, name: res.show.name ?? "", imageURL: res.show.image?.medium ?? "", rating: nil, premiered: res.show.premiered, summary: res.show.summary ?? "", cast: self.cast)
                                self.searchResults.append(newShow)
                                self.cast = []
                            }
                        }
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }
            })
        }
    }
    
    func fetchCast(showID: Int) {
        self.cast = []
        self.group.enter()
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.castAPIService.fetchCastData(showID: showID, completionHandler: { [self] response in
                DispatchQueue.main.async {
                    switch response {
                    case.success(let result):
                        var counter = 0
                        for res in result {
                            let id = showID + counter
                            let newActor = CastModel(id: id, realName: res.person.name, characterName: res.character.name, imageURL: res.person.image.medium)
                            self.cast.append(newActor)
                            counter += 1
                        }
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }
                self.group.leave()
            })
        }
    }
    
    func getShow(id: Int) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.showAPISerivce.fetchShowData(showID: id, completionHandler: { [self] response in
                DispatchQueue.main.async {
                    switch response {
                    case.success(let result):
                        let summaryWithoutTags = result.summary.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
                        let newShow = ShowModel(id: result.id, name: result.name, imageURL: result.image.medium, rating: result.rating.average, premiered: nil, summary: summaryWithoutTags, cast: nil)
                        self.show = newShow
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }
            })
        }
    }

}
