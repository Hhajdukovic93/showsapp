//
//  SearchCoordinator.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 26.10.2022..
//

import UIKit
import SwiftUI

class SearchCoordinator: Coordinator {
    
    func start() -> UIViewController {
        let vc = UIHostingController(rootView: SearchView())
        vc.tabBarItem.title = "Search"
        vc.tabBarItem.image = UIImage(systemName: "magnifyingglass.circle.fill")
        return vc
    }
    
}
