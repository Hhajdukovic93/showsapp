//
//  SearchCardView.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import SwiftUI

struct SearchCardView: View {
    var imageURL: String
    var name: String
    var premiered: String
    private var year: String {
        var result = ""
        if premiered != "" {
            let index = premiered.index(premiered.startIndex, offsetBy: 4)
            result = String(premiered[..<index])
        }
        return result
    }
    var cast: [CastModel]?
    var actorsAsString: String {
        var result = ""
        if let castData = cast {
            for person in castData {
                let name = person.realName
                result += "\(name), "
            }
        }
        return result
    }
    
    var body: some View {
        HStack() {
            AsyncImage(url: URL(string: imageURL) ) { image in
                image
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    
            } placeholder: {
                Color.gray
            }
            .frame(width: 55, height: 77)
            .padding(.leading)
            VStack(alignment: .leading) {
                Text(name)
                    .foregroundColor(.white)
                Text(year)
                    .foregroundColor(.gray)
                Text(actorsAsString)
                .foregroundColor(.gray)
                .lineLimit(1)
            }
            Spacer()
        }
        .background(.black)
    }
}

struct SearchCardView_Previews: PreviewProvider {
    static var previews: some View {
        SearchCardView(imageURL: "https://static.tvmaze.com/uploads/images/medium_portrait/81/202627.jpg" ,name: "The Margin Call", premiered: "2022-01-02")
    }
}
