//
//  SearchView.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 26.10.2022..
//

import SwiftUI

struct SearchView: View {
    @StateObject private var searchVM = SearchViewModel(showAPISerivce: ShowAPIService(), searchAPIService: SearchAPIService(), castAPIService: CastAPIService())
    @State var searchText = ""
    
    var body: some View {
        VStack {
            HStack {
                Image(systemName: "magnifyingglass")
                    .foregroundColor(.gray)
                    .onTapGesture {
                        searchVM.search(text: searchText)
                    }
                TextField("search", text: $searchText)
                    .disableAutocorrection(true)
            }
            .frame(height: 30)
            .padding(7)
            .background(
                RoundedRectangle(cornerRadius: 15)
                    .fill(.white)
            )
            .padding()
            ScrollView(.vertical) {
                VStack() {
                    ForEach(searchVM.searchResults) { show in
                        NavigationLink {
                            DetailsView(id: show.id)
                        } label: {
                            SearchCardView(imageURL: show.imageURL, name: show.name, premiered: show.premiered ?? "", cast: show.cast)
                        }
                    }
                }
            }
        }
        .background(.black)
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}
