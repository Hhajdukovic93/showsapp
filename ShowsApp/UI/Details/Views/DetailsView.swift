//
//  DetailsView.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import SwiftUI

struct DetailsView: View {
    @ObservedObject var favsManager = FavoritesManager.shared
    @StateObject private var searchVM = SearchViewModel(showAPISerivce: ShowAPIService(), searchAPIService: SearchAPIService(), castAPIService: CastAPIService())
    var id: Int
    
    var body: some View {
        ZStack {
            Color(.black)
            VStack {
                ScrollView(.vertical) {
                    ZStack(alignment: .topTrailing) {
                        AsyncImage(url: URL(string: searchVM.show.imageURL)) { image in
                            image
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                        } placeholder: {
                            Color.gray
                        }
                        ZStack {
                            Rectangle()
                                .frame(width: 44, height: 44)
                                .overlay(RoundedRectangle(cornerRadius: 5).stroke(Color.gray, lineWidth: 2))
                            if favsManager.isFavorite(favorite: id) {
                                Image(systemName: "heart.fill")
                                    .foregroundColor(.yellow)
                            } else {
                                Image(systemName: "heart.fill")
                                    .foregroundColor(.gray)
                            }
                        }
                        .foregroundColor(.black)
                        .onTapGesture {
                            favsManager.isFavorite(favorite: id) ? favsManager.removeFavorite(favorite: id) : favsManager.addFavorite(favorite: id)
                        }
                    }
                    Spacer()
                    VStack {
                        Text(searchVM.show.summary ?? "")
                            .foregroundColor(.gray)
                        HStack {
                            Text("Cast")
                                .font(.title3)
                                .foregroundColor(.gray)
                            Spacer()
                            Text("show all")
                                .foregroundColor(.yellow)
                                .padding(.trailing)
                        }
                        .padding(.top)
                        ScrollView(.horizontal) {
                            HStack() {
                                ForEach(searchVM.cast) { character in
                                    CastView(imageURL: character.imageURL, realName: character.realName, characterName: character.characterName)
                                }
                            }
                        }
                    }
                    Spacer()
                }
            }
        }
        .onAppear {
            Task {
                searchVM.getShow(id: id)
                searchVM.fetchCast(showID: id)
            }
        }
        .edgesIgnoringSafeArea(.bottom)
    }
}

struct DetailsView_Previews: PreviewProvider {
    static var previews: some View {
        DetailsView(id: 1)
    }
}
