//
//  CastView.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 27.10.2022..
//

import SwiftUI

struct CastView: View {
    var imageURL: String
    var realName: String
    var characterName: String
    
    var body: some View {
        VStack {
            AsyncImage(url: URL(string: imageURL) ) { image in
                image
                    .resizable()
                    .aspectRatio(contentMode: .fill)
            } placeholder: {
                Color.gray
            }
            Text(realName)
                .foregroundColor(.white)
                .font(.caption)
                .lineLimit(1)
            Text(characterName)
                .foregroundColor(.gray)
                .font(.caption2)
                .lineLimit(1)
        }
        .background(.black)
        .frame(width: 94, height: 166)
    }
}

struct CastView_Previews: PreviewProvider {
    static var previews: some View {
        CastView(imageURL: "https://static.tvmaze.com/uploads/images/medium_portrait/0/1815.jpg", realName: "Mike Vogel", characterName: "Dale Barbara")
    }
}
