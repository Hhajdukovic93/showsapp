//
//  RootCoordinator.swift
//  ShowsApp
//
//  Created by Hrvoje Hajdukovic on 26.10.2022..
//

import Foundation
import UIKit

class RootCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    let tabBarController = UITabBarController()
    let navigationController = UINavigationController()
    
    func start() -> UIViewController {
        childCoordinators = [HomeCoordinator(), SearchCoordinator(), FavoritesCoordinator()]
        
        tabBarController.viewControllers = childCoordinators.map({ coordinator in
            coordinator.start()
        })
        tabBarController.tabBar.barTintColor = .black
        tabBarController.tabBar.tintColor = .yellow

        navigationController.viewControllers = [tabBarController]
        
        return navigationController
    }
    
}
